import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
			title: "Zuitt Coding Bootcamp",
			content: "Opportunities for everyone, everywhere.",
			destination: "/courses",
			label: "Enroll Now!"
		}
	// console.log(typeof data)

	return (

		<>
			<Banner data={data}/>
			<Highlights />
			{/*<CourseCard />*/}
		</>

	)
}