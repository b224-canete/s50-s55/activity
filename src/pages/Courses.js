import { useState, useEffect } from 'react';
import CourseCard from '../components/CourseCard';
// import coursesData from '../data/coursesData';

export default function Courses() {

	// Checks to see if the mock data was captured
	// console.log(coursesData);
	// console.log(coursesData[0]);

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} courseProp={course} />
				)
			}))
		})
	}, [])



/*	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course} />
		)
	})*/

	return (
		<>
			<h1>Courses</h1>
			{courses}
			{/*or CourseCard courseProp={coursesData[0]}*/}
		</>
	)
}