import { useState, useEffect, useContext } from 'react';
import { Col, Row, Form, Button, Container } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {

	const { user } = useContext(UserContext)

	const navigate = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	// State to determine whether the submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	


	// Function to simulate user registration
	function registerUser(e) {
		e.preventDefault()


		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			{
				(data === true) ?
				Swal.fire({
					title: "Duplicate Email Found",
					icon: "error",
					text: "Please provide a different email."
				})
				:
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data)
					if(data === true) {
						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						navigate("/login")

					} else{
						Swal.fire({
							title: "Registration Failed",
							icon: "error",
							text: "Please try again."
						})

					}
				})
			}
		})
	};



// Clear the input fields and states 
// setEmail("");
// setPassword1("");
// setPassword2("");

	useEffect(() => {
		if((
			firstName !== "" && 
			lastName !== "" &&
			email !== "" && 
			password1 !== "" && 
			password2 !== "" &&
			mobileNo.length >= 11 && 
			mobileNo !== "") && 
			(password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, password1, password2, mobileNo]);


	return (

		(user.id !== null) ?
			<Navigate to="/courses" />
			:
			<Container>
				<Row>
					<Col lg={{span: 4, offset: 4}}>
						<h1 className="text-center my-3">Register</h1>
						<Form onSubmit={e => registerUser(e)}>

							<Form.Group className="mb-2" controlId="userFirstName">
							  <Form.Label>First Name</Form.Label>
							  <Form.Control 
							  	type="text" 
							  	placeholder="Enter first name"
							  	value={firstName}
							  	onChange={e => setFirstName(e.target.value)}
							  	required
							  />
							</Form.Group>

							<Form.Group className="mb-2" controlId="userLastName">
							  <Form.Label>Last Name</Form.Label>
							  <Form.Control 
							  	type="text" 
							  	placeholder="Enter last name"
							  	value={lastName}
							  	onChange={e => setLastName(e.target.value)}
							  	required
							  />
							</Form.Group>

							<Form.Group className="mb-2" controlId="userEmail">
							<Form.Label>Email address</Form.Label>
							<Form.Control 
								type="email" 
								placeholder="Enter email"
								value={email}
								onChange={e => setEmail(e.target.value)}
								required
							/>
							</Form.Group>

							<Form.Group className="mb-2" controlId="password1">
							<Form.Label>Password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Password"
								value={password1}
								onChange={e => setPassword1(e.target.value)}
								required 
							/>
							</Form.Group>

							<Form.Group className="mb-2" controlId="password2">
							<Form.Label>Verify Password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Verify Password"
								value={password2}
								onChange={e => setPassword2(e.target.value)}
								required 
							/>
							</Form.Group>

							<Form.Group className="mb-2" controlId="userMobileNo">
							  <Form.Label>Mobile No</Form.Label>
							  <Form.Control 
							  	type="text" 
							  	placeholder="Enter mobile no."
							  	value={mobileNo}
							  	onChange={e => setMobileNo(e.target.value)}
							  	required
							  />
							</Form.Group>

							{
								isActive ?
									<Button variant="primary" type="submit" id="submitBtn">
									  Submit
									</Button>
									:
									<Button variant="primary" type="submit" id="submitBtn" disabled>
									  Submit
									</Button>

							}
						</Form>
					</Col>
				</Row>
			</Container>

	)
}